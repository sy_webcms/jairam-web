$(document).ready(function(){
	$(document).on("click",".menuIcon",function(){
		$(this).toggleClass("change");
	});

  var nav_link = $('.nav-link').not('.course_nav .nav-link');
  //$(document).on("click",".nav-link", function(){
    $(nav_link).on('click', function(event) {
    $(".toggle_menu_btn").not($(this).children(".toggle_menu_btn")).removeClass("minus");
    $(this).children(".toggle_menu_btn").toggleClass("minus");
    $(".submenu").not($(this).next(".submenu")).slideUp();
    $(this).next(".submenu").slideToggle();
    
    var window_width = $(window).width();
    if (window_width <= 992) {
      $('html, body').animate({
          //scrollTop: $("#tab_parent").offset().top - $(".page_banner").parent("section").height() - $(".sticky_courses .nav").height() - 5 
          scrollTop: $("#tab_parent").offset().top - $(".header_top").height() - 5 
          //scrollTop: $("#tab_parent").offset().top - $(".page_banner").parent("section").height()  
      }, 700);
    }
    else{
      $('html, body').animate({
        scrollTop: $("#tab_section").offset().top - $(".header_top").height() - $(".menu").height() - 5 
    }, 700);
    }

    
  });

  $(document).on('click', '.nav-item .submenu a', function (event) {
    //event.preventDefault();
    $('html, body').animate({
        scrollTop: $($.attr(this, 'href')).offset().top - 170
    }, 700);
  });

  $(document).on('click', '.menuIcon', function () {
    var window_width = $(window).width();
    $(".more_menu").slideToggle(200);
    if (window_width <= 992) {
          $(".menu").slideToggle();
      } 
  });






  function menuFixed(){
      var scrollTop = $(window).scrollTop();
      var window_width = $(window).width();
      if (scrollTop > 100 || (window_width < 992 &&  scrollTop > 10) ) {
          $("header").addClass("active");
      } else {
          $("header").removeClass("active");
      }

      
      if (scrollTop > 300 || (window_width < 992 &&  scrollTop > 10) ) {
          $(".floating_form").addClass("active");
      } else {
          $(".floating_form").removeClass("active");
      }

      
    }
    $(window).on("scroll", function() {
        menuFixed();
        sticky_courses();
    });
  menuFixed();


  function sticky_courses(){
    var scrollTop = $(window).scrollTop();
      var window_width = $(window).width()
      if (scrollTop > 550 || (window_width < 992 &&  scrollTop > 10) ) {
          $(".sticky_courses").addClass("sticky");
      } else {
          $(".sticky_courses").removeClass("sticky");
      }
  }

  $(document).on('click', '.has-sub .dropdown-toggle', function(event) {
            event.preventDefault();
            $(this).parent().next("ul").slideToggle();
            $(this).parent().toggleClass("current");
        });
  

  function hashFunc(){
    $(function(){
        var hash = window.location.hash;
        hash && $('ul.nav a[href="' + hash + '"]').tab('show');
        console.log("hash");
        console.log(hash);
          
          if(hash !=""){
              var window_width = $(window).width();
              if (window_width <= 992) {
                $('html, body').animate({
                    //scrollTop: $("#tab_parent").offset().top - $(".page_banner").parent("section").height() - $(".sticky_courses .nav").height() - 5 
                    scrollTop: $("#tab_parent").offset().top - $(".header_top").height() - 5 
                    //scrollTop: $("#tab_parent").offset().top - $(".page_banner").parent("section").height()  
                }, 700);
              }
              else{
                $('html, body').animate({
                  scrollTop: $("#tab_section").offset().top - $(".header_top").height() - $(".menu").height() - 5 
              }, 700);
              }
          }


      });
  }

  setTimeout(function(){
      hashFunc();
  },800);

  $(document).on("click",".floating_form .closeIcon", function(){
    $(".floating_form").hide();
  });

  $(document).on("click",".menu .has-sub ul li a, .quick_links .cols ul li a", function(){
    hashFunc()
  });

  $(document).on("click",".toggle_click", function(){
    $(this).parent().next(".toggle_div").slideToggle();
  });

});